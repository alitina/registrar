import { BuyActionsSharedModule } from './buy-actions-shared/buy-actions-shared.module';

describe('BuyActionsSharedModule', () => {
  let buyActionsSharedModule: BuyActionsSharedModule;

  beforeEach(() => {
    buyActionsSharedModule = new BuyActionsSharedModule();
  });

  it('should create an instance', () => {
    expect(buyActionsSharedModule).toBeTruthy();
  });
});
