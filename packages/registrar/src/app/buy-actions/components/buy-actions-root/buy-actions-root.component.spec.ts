import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyActionsRootComponent } from './buy-actions-root.component';

describe('BuyActionsRootComponent', () => {
  let component: BuyActionsRootComponent;
  let fixture: ComponentFixture<BuyActionsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyActionsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyActionsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
