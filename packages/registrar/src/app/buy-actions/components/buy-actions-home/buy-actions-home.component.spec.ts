import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyActionsHomeComponent } from './buy-actions-home.component';

describe('BuyActionsHomeComponent', () => {
  let component: BuyActionsHomeComponent;
  let fixture: ComponentFixture<BuyActionsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyActionsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyActionsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
