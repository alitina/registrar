import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { RouterModalModule } from '@universis/common/routing';
import { AdvancedFormsModule } from '@universis/forms';
import { environment } from '../../environments/environment';

import * as en from './i18n/buyActions.en.json';
import * as el from './i18n/buyActions.el.json';

const translations = {
  en,
  el
};

@NgModule({
  imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        MostModule,
        RouterModalModule,
        AdvancedFormsModule,
  ],
  declarations: []
})
export class BuyActionsSharedModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BuyActionsSharedModule,
      providers: [
      ]
    };
  }

  constructor(private _translateService: TranslateService) {
    
    environment.languages.forEach((language: string) => {
      if (Object.prototype.hasOwnProperty.call(translations, language)) {
        this._translateService.setTranslation(language, translations[language], true);
      }
    });

  }

}