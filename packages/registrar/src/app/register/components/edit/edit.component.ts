import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ModalService, DIALOG_BUTTONS, LoadingService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';

const EXPAND_INTERNSHIP_REGISTRATIONS = 'internshipRegistrations($select=id,initiator,actionStatus,internship/name as internshipName,internship/company/name as companyName,internship/company/country/name as countryName;$expand=actionStatus,review($expand=createdBy))';

@Component({
  selector: 'app-edit-register-action',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit, OnDestroy {



  public static readonly ServiceQueryParams = {
    $expand: 'studyProgram($expand=feeCurrency),specialization,inscriptionYear,inscriptionPeriod($expand=locale),messages,review($expand=createdBy),candidate($expand=person($expand=gender($expand=locale))),attachments($expand=attachmentType),discount($expand=locale),educationLevel($expand=locale),employmentStatus($expand=locale)'
  }

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public messages: any[];
  public otherApplications: any[];
  newMessage: any = {
    attachments: []
  };
  public showNewMessage = false;
  public activeCourseRegistration: any;
  public activeInternshipRegistration: any;
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      if (query && query.target) {
        this.activeCourseRegistration = parseInt(query.target, 10);
      } else {
        this.activeCourseRegistration = null;
      }
      if (query && query.targetInternship) {
        this.activeInternshipRegistration = parseInt(query.targetInternship, 10);
      } else {
        this.activeInternshipRegistration = null;
      }
    });
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this.model = data.model;
      if (this.form) {
        this.form.refreshForm.emit({
          submission: {
            data: this.model
          }
        });
      }
      // get messages
      if (this.model) {

        // get other active applications of the same user
        this._context.model('StudyProgramRegisterActions')
          .where('inscriptionYear').equal(this.model.inscriptionYear.id)
          .and('inscriptionPeriod').equal(this.model.inscriptionPeriod.id)
          .and('owner').equal(this.model.owner)
          .and('actionStatus/alternateName').equal('ActiveActionStatus')
          .expand('studyProgram', 'specialization')
          .orderByDescending('dateModified')
          .getItems().then((results) => {
            this.otherApplications = results.filter((item) => {
              return item.id !== this.model.id;
            });
          });

        this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`)
          .asQueryable()
          .orderBy('dateCreated desc')
          .expand('attachments')
          .getItems().then((results) => {
            this.messages = results;
          }).catch((err) => {
            console.error(err);
          });
      }
    });
  }

  reload() {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('StudyProgramRegisterActions')
      .asQueryable(<DataServiceQueryParams>EditComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
      });
  }

  preview(attachment: any) {
    return this._router.navigate(['.'], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: true,
      queryParams: {
        download: attachment.url
      }
    });
  }

  download(attachment: any) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  accept() {
    const AcceptConfirm = this._translateService.instant('Register.AcceptConfirm') || {
      Title: 'Accept',
      Message: 'You are going to finally accept this application. Do you want to proceed?'
    };
    this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              "Title": "Refresh failed",
              "Message": "The operation has been completed successfully but something went wrong during refresh."
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  acceptCourseRegistration(courseRegistration) {
    const AcceptConfirm = this._translateService.instant('Register.AcceptCourseRegistration') || {
      "Title": "Accept course registration",
      "Message": "You are going to accept the selected course registration. Do you want to proceed?"
    };
    this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('CourseClassRegisterActions').save({
          id: courseRegistration.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        }).then((result) => {
          // set data
          courseRegistration.actionStatus = {
            alternateName: 'CompletedActionStatus'
          };
          // send an application event
          this._appEvent.change.next({
            model: 'CourseClassRegisterActions',
            target: courseRegistration
          });

          this._loading.hideLoading();
          this._appEvent.change.next({
            model: 'StudyProgramRegisterActions',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  acceptInternshipRegistration(courseRegistration) {
    const AcceptConfirm = this._translateService.instant('Register.AcceptInternshipRegistration') || {
      "Title": "Accept internship registration",
      "Message": "You are going to accept the selected internship registration. Do you want to proceed?"
    };
    this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('InternshipRegisterActions').save({
          id: courseRegistration.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        }).then((result) => {
          // set data
          courseRegistration.actionStatus = {
            alternateName: 'CompletedActionStatus'
          };
          // send an application event
          this._appEvent.change.next({
            model: 'InternshipRegisterActions',
            target: courseRegistration
          });

          this._loading.hideLoading();
          this._appEvent.change.next({
            model: 'StudyProgramRegisterActions',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  rejectCourseRegistration(courseRegistration) {
    const RejectConfirm = this._translateService.instant('Register.RejectCourseRegistration') || {
      "Title": "Reject course registration",
      "Message": "You are going to reject the selected course registration. Do you want to proceed?"
    };
    this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('CourseClassRegisterActions').save({
          id: courseRegistration.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        }).then((result) => {
          // set data
          courseRegistration.actionStatus = {
            alternateName: 'CancelledActionStatus'
          };
          // send an application event
          this._appEvent.change.next({
            model: 'CourseClassRegisterActions',
            target: courseRegistration
          });
          this._loading.hideLoading();
          this._appEvent.change.next({
            model: 'StudyProgramRegisterActions',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  rejectInternshipRegistration(courseRegistration) {
    const RejectConfirm = this._translateService.instant('Register.RejectInternshipRegistration') || {
      "Title": "Reject internship registration",
      "Message": "You are going to reject the selected internship registration. Do you want to proceed?"
    };
    this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('InternshipRegisterActions').save({
          id: courseRegistration.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        }).then((result) => {
          // set data
          courseRegistration.actionStatus = {
            alternateName: 'CancelledActionStatus'
          };
          // send an application event
          this._appEvent.change.next({
            model: 'InternshipRegisterActions',
            target: courseRegistration
          });
          this._loading.hideLoading();
          this._appEvent.change.next({
            model: 'StudyProgramRegisterActions',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reject() {
    const RejectConfirm = this._translateService.instant('Register.RejectConfirm') || {
      Title: 'Reject and cancel',
      Message: 'You are going to reject this application. Do you want to proceed?'
    };
    this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              "Title": "Refresh failed",
              "Message": "The operation has been completed successfully but something went wrong during refresh."
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        })
      }
    });
  }

  revert() {
    const RevertConfirm = this._translateService.instant('Register.RevertConfirm') || {
      Title: 'Activate application',
      Message: 'You are going to activate this application. Do you want to proceed?'
    };
    this._modal.showDialog(RevertConfirm.Title, RevertConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'ActiveActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              "Title": "Refresh failed",
              "Message": "The operation has been completed successfully but something went wrong during refresh."
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        })
      }
    });
  }

  reset() {
    const ResetConfirm = this._translateService.instant('Register.ResetConfirm') || {
      Title: 'Change application status',
      Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
    };
    this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'PotentialActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              "Title": "Refresh failed",
              "Message": "The operation has been completed successfully but something went wrong during refresh."
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          });
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        })
      }
    });
  }

  onEditReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`CourseClassRegisterActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        this.editingReview = false;
        // update data
        const findItem = this.model.courseRegistrations.find((item) => {
          return item.id === result.itemReviewed;
        });
        if (findItem) {
          if (findItem.review) {
            Object.assign(findItem.review, result);
          } else {
            findItem.review = result;
          }
          // send an application event
          this._appEvent.change.next({
            model: 'CourseClassRegisterActions',
            target: findItem
          });
        }

      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onItemReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`StudyProgramRegisterActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        return this._context.model(`StudyProgramRegisterActions/${event.data.itemReviewed}/review`)
          .asQueryable().expand('createdBy', 'modifiedBy').getItem().then((finalResult) => {
            this.editingReview = false;
            this.model.review = finalResult;
          });
      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onInternshipEditReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`InternshipRegisterActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        this.editingReview = false;
        // update data
        const findItem = this.model.internshipRegistrations.find((item) => {
          return item.id === result.itemReviewed;
        });
        if (findItem) {
          if (findItem.review) {
            Object.assign(findItem.review, result);
          } else {
            findItem.review = result;
          }
          // send an application event
          this._appEvent.change.next({
            model: 'InternshipRegisterActions',
            target: findItem
          });
        }
      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onMessageEvent(event) {
    if (event.data && event.data.cancelMessage) {
      this.showNewMessage = false;
      this.newMessage = {};
    }
  }

  async sendWithoutAttachment(message) {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: this.model.owner
      });
      await this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`).save(message);
      // reload message
      this.messages = await this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async send(message) {
    try {
      if (!(message.attachments && message.attachments.length)) {
        // send message without attachment
        await this.sendWithoutAttachment(message);
        return;
      }
      this._loading.showLoading();

      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: this.model.owner
      });

      const formData: FormData = new FormData();
      // get attachment if any
      if (message.attachments && message.attachments.length) {
        formData.append('attachment', message.attachments[0], message.attachments[0].name);
      }
      Object.keys(message).filter((key) => {
        return key !== 'attachments';
      }).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(message, key)) {
          formData.append(key, message[key]);
        }
      });
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const serviceUrl = this._context.getService().resolve(`StudyProgramRegisterActions/${this.model.id}/sendMessage`);
      await this._http.post(serviceUrl, formData, {
        headers: serviceHeaders
      }).toPromise();
      // reload message
      this.messages = await this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }

  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status != 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

}
