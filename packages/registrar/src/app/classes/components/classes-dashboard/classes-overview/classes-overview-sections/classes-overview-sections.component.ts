import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-sections',
  templateUrl: './classes-overview-sections.component.html',
  styleUrls: ['./classes-overview-sections.component.scss']
})
export class ClassesOverviewSectionsComponent implements OnInit, OnDestroy  {

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext, 
    private _templateManageService: TemplateManageService
  ) { }

  public sections;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;
  public specialization: any;
  public studyProgram: any;

  async ngOnInit() {
    this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.specialization = params.specialization;
      this.sections = await this._context.model('CourseClassSections')
        .where('courseClass').equal(params.id)
        .expand('instructor1,instructor2,instructor3')
        .getItems();
      // instructors are not fetched correctly, change this when fetching is fixed at server side
      let totals = [];
      if (this.sections.length > 0) {
        // get total students per section
        totals = await this._context.model('StudentCourseClasses')
          .where('courseClass').equal(params.id)
          .select('section,count(id) as total')
          .groupBy('section')
          .getItems();
      }
      this.sections.map(x => {
        x.instructors = [];
        if (x.instructor1) {
          x.instructors.push(x.instructor1);
        }
        if (x.instructor2) {
          x.instructors.push(x.instructor2);
        }
        if (x.instructor3) {
          x.instructors.push(x.instructor3);
        }
        // map totals
        const section = totals.find(y => {
          return y.section === x.section;
        });
        x.totalStudents = section ? section.total : 0;
        return x;
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
  }
}
