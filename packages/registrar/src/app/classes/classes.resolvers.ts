import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class CourseClassTimetableResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<number> | Observable<number> {
    const { id } = route.params;
    return from(
        this.context.model('CourseClasses').where('id').equal(id).select('studyProgram/id as studyProgram').getItem()
    ).pipe(switchMap(({studyProgram}) => {
        return from(this.context.model('TimetableEvents').where('studyPrograms/id').equal(studyProgram).select('id').getItem())
    })).map(timetableEvent => timetableEvent ? timetableEvent.id : null);
  }
}

@Injectable()
export class CurrentCourseClassResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<number> | Observable<number> {
    const { id } = route.params;
    return id;
  }
}

@Injectable()
export class NewEventCourseClassResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | Observable<any> {
    const { id } = route.params;
    return from(
      this.context.model('CourseClasses')
            .where('id').equal(id)
            .expand('course')
            .getItem()
          );
  }
}

@Injectable()
export class NewEventInstructorResolver implements Resolve<any> {
  constructor(private context: AngularDataContext) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | Observable<any> {
    const { id } = route.params;
    return from(
      this.context.model('CourseClassInstructors')
            .where('courseClass').equal(id)
            .expand('instructor')
            .getItem().then(({instructor}) => {
              return instructor;
            })
          );
  }
}