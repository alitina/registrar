export interface Instructor {
    familyName: string;
    givenName: string;
    id: number;
    departmentId: number;
    departmentName: string;
    label: string;
    category: string;
    email: string;
    instructorCode: string;
    type: string;
  }
