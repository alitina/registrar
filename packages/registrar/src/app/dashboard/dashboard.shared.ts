import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

import { DashboardQuickSearchComponent } from './components/dashboard-quick-search/dashboard-quick-search.component';
import { DashboardQuickActionsComponent } from './components/dashboard-quick-actions/dashboard-quick-actions.component';
// tslint:disable-next-line:max-line-length
import { DashboardPendingStudentRequestsComponent } from './components/dashboard-pending-student-requests/dashboard-pending-student-requests.component';
import { DashboardExamDocumentsComponent } from './components/dashboard-exam-documents/dashboard-exam-documents.component';
import {RouterModule} from '@angular/router';
// tslint:disable-next-line:max-line-length
import {DashboardExamParticipateSummaryComponent} from './components/dashboard-exam-participate-summary/dashboard-exam-participate-summary.component';
import { SharedModule } from '@universis/common';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
   DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent,
    DashboardExamParticipateSummaryComponent
  ],
  exports: [
    DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent,
    DashboardExamParticipateSummaryComponent
  ],
  providers: [
    DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent,
    DashboardExamParticipateSummaryComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})

export class DashboardSharedModule implements OnInit {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading dashboard module');
      console.error(err);
    });
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`./i18n/dashboard.${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }
}
