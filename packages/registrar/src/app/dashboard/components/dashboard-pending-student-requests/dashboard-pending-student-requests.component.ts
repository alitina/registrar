import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActiveDepartmentService} from "../../../registrar-shared/services/activeDepartmentService.service";
import { from, Observable } from 'rxjs';
import { first, map, shareReplay, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-pending-student-requests',
  templateUrl: './dashboard-pending-student-requests.component.html'
})
export class DashboardPendingStudentRequestsComponent implements OnInit {

  public lastRequests$: Observable<any>;

  constructor(private context: AngularDataContext,
              private activeDepartmentService: ActiveDepartmentService ) { }

  ngOnInit() {
    this.lastRequests$ = this.activeDepartmentService.departmentChange.pipe(switchMap((department: any) => {
      return from(this.context.model('StudentRequestActions')
      .where('actionStatus/alternateName ').equal('ActiveActionStatus')
      .and('student/department').equal(department.id)
      .select(
        'id',
        'description',
        'name',
        'student/studentIdentifier as studentIdentifier',
        'student/person/givenName as givenName',
        'student/person/familyName as familyName',
        'student/studyProgram/name as studyProgram',
        'dateModified'
      )
      .orderByDescending('dateModified')
      .take(5).getItems());
    }), shareReplay(1));
    
  }

}
