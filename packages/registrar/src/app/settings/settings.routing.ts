import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ListComponent, EnumerationModelFormResolver, EnumerationModalTitleResolver} from './components/list/list.component';
import {SectionsComponent} from './components/sections/sections.component';
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver} from '@universis/forms';
import {AdvancedFormModelResolver} from '@universis/forms';
import {AdvancedListComponent} from '@universis/ngx-tables';
import {AdvancedTableConfigurationResolver} from '@universis/ngx-tables';
import {AdvancedFormItemWithLocalesResolver} from '@universis/forms';
import {ActiveDepartmentIDResolver} from '../registrar-shared/services/activeDepartmentService.service';
import * as DepartmentCourseAreasConfig from './components/list/courseAreas.config.list.json';
import * as DepartmentCourseSectorsConfig from './components/list/courseSectors.config.list.json';
import * as DepartmentExamPeriodsConfig from './components/list/examPeriods.config.json';
import * as DepartmentAcademicPeriodsConfig from './components/list/academicPeriods.config.json';
import * as DepartmentThesisSubjectsConfig from './components/list/thesisSubjects.config.list.json';
import * as CandidateSourcesConfig from './components/list/candidateSources.config.list.json';
import * as NationalitiesSourcesConfig from './components/list/nationalities.config.list.json';
import * as ActionStatusArticlesConfig from './components/list/actionStatusArticles.config.list.json';
import * as GradeScaleListConfig from './gradeScales.config.list.json';
import * as DiscountCategoriesConfig from './components/list/discountCategories.config.list.json';
import { GradeScaleWithLocalesResolver } from './gradeScale.resolver';
import {
  CandidateSourcesAttachSchemaComponent
} from './components/candidate-sources/candidate-sources-attach-schema/candidate-sources-attach-schema.component';
import * as DepartmentAcademicYearsConfig from './components/list/academicYears.config.json';
import { AdvancedListWrapperComponent } from './components/candidate-sources/advanced-list-wrapper/advanced-list-wrapper.component';


const routes: Routes = [
  {
    path: 'configuration/GradeScales',
    component: AdvancedListComponent,
    data: {
      model: 'GradeScales',
      tableConfiguration: GradeScaleListConfig,
      description: 'Settings.Lists.GradeScale.Description',
      longDescription: 'Settings.Lists.GradeScale.LongDescription',
      category: 'Settings.Title'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true,
          serviceQueryParams: {
            $expand: 'locales,values($expand=locales)'
          }
        },
        resolve: {
          data: GradeScaleWithLocalesResolver
        }
      }
    ]
  },
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Settings'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'sections'
      },
      {
        path: 'sections',
        component: SectionsComponent
      },
      {
        path: 'lists/AttachmentTypes',
        component: AdvancedListComponent,
        data: {
          model: 'AttachmentTypes',
          list: 'active',
          description: 'Settings.Lists.AttachmentType.Description',
          longDescription: 'Settings.Lists.AttachmentType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseTypes',
        component: AdvancedListComponent,
        data: {
          model: 'CourseTypes',
          list: 'active',
          description: 'Settings.Lists.CourseTypes.Description',
          longDescription: 'Settings.Lists.CourseTypes.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales'
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseAreas',
        component: AdvancedListComponent,
        data: {
          model: 'CourseAreas',
          list: 'active',
          description: 'Settings.Lists.CourseAreas.Description',
          longDescription: 'Settings.Lists.CourseAreas.LongDescription',
          tableConfiguration: DepartmentCourseAreasConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseAreas',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseAreas',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/Places',
        component: AdvancedListComponent,
        data: {
          model: 'Places',
          list: 'places',
          description: 'Settings.Lists.Places.Description',
          longDescription: 'Settings.Lists.Places.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Places',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Places',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseSectors',
        component: AdvancedListComponent,
        data: {
          model: 'CourseSectors',
          list: 'active',
          description: 'Settings.Lists.CourseSectors.Description',
          longDescription: 'Settings.Lists.CourseSectors.LongDescription',
          tableConfiguration: DepartmentCourseSectorsConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseSectors',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseSectors',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/ExamPeriods',
        component: AdvancedListComponent,
        data: {
          model: 'ExamPeriods',
          list: 'active',
          description: 'Settings.Lists.ExamPeriods.Description',
          longDescription: 'Settings.Lists.ExamPeriods.LongDescription',
          tableConfiguration: DepartmentExamPeriodsConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExamPeriods',
              action: 'new',
              closeOnSubmit: true,
              data: {
                '$state': 1
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExamPeriods',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales, academicPeriods($select=id,locale/name)',
                $filter: 'locale/inLanguage eq lang()'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/AcademicPeriods',
        component: AdvancedListComponent,
        data: {
          model: 'AcademicPeriods',
          list: 'active',
          description: 'Settings.Lists.AcademicPeriods.Description',
          longDescription: 'Settings.Lists.AcademicPeriods.LongDescription',
          serviceQueryParams: {
            $orderBy: 'alternateName asc',
          },
          tableConfiguration: DepartmentAcademicPeriodsConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicPeriods',
              action: 'new',
              closeOnSubmit: true,
              data: {
                '$state': 1
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicPeriods',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/DiscountCategories',
        component: AdvancedListComponent,
        data: {
          model: 'DiscountCategories',
          list: 'active',
          description: 'Settings.Lists.DiscountCategory.Description',
          longDescription: 'Settings.Lists.DiscountCategory.LongDescription',
          serviceQueryParams: {
            $orderBy: 'alternateName asc',
          },
          tableConfiguration: DiscountCategoriesConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiscountCategories',
              action: 'new',
              closeOnSubmit: true,
              data: {
                '$state': 1
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiscountCategories',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/ThesisSubjects',
        component: AdvancedListComponent,
        data: {
          model: 'ThesisSubjects',
          list: 'active',
          description: 'Settings.Lists.ThesisSubjects.Description',
          longDescription: 'Settings.Lists.ThesisSubjects.LongDescription',
          tableConfiguration: DepartmentThesisSubjectsConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ThesisSubjects',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ThesisSubjects',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/AcademicYears',
        component: AdvancedListComponent,
        data: {
          model: 'AcademicYears',
          list: 'active',
          description: 'Settings.Lists.AcademicYear.LongDescription',
          longDescription: 'Settings.Lists.AcademicYear.LongDescription',
          tableConfiguration: DepartmentAcademicYearsConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicYears',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicYears',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/StudyLevels',
        component: AdvancedListComponent,
        data: {
          model: 'StudyLevels',
          list: 'index',
          description: 'Settings.Lists.StudyLevel.Description',
          category: 'Settings.Title'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              modalOptions: EnumerationModalTitleResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales'
              }
            },
            resolve: {
              data: AdvancedFormItemWithLocalesResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          }
        ]
      },
      {
        path: 'lists/ActionStatusArticles',
        component: AdvancedListComponent,
        data: {
          model: 'ActionStatusArticles',
          list: 'active',
          description: 'Settings.Lists.ActionStatusArticles.Description',
          longDescription: 'Settings.Lists.ActionStatusArticles.LongDescription',
          tableConfiguration: ActionStatusArticlesConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ActionStatusArticles',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ActionStatusArticles',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'actionStatus'
              },
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/CandidateSources',
        component: AdvancedListWrapperComponent,
        data: {
          model: 'CandidateSources',
          list: 'active',
          description: 'Settings.Lists.CandidateSource.Description',
          longDescription: 'Settings.Lists.CandidateSource.LongDescription',
          tableConfiguration: CandidateSourcesConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CandidateSources',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CandidateSources',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          },
          {
            path: ':id/attachSchema',
            pathMatch: 'full',
            component: CandidateSourcesAttachSchemaComponent,
            outlet: 'modal'
          }
        ]
      },
      {
        path: 'lists/Nationalities',
        component: AdvancedListComponent,
        data: {
          model: 'Nationalities',
          list: 'active',
          description: 'Settings.Lists.Nationalities.Description',
          longDescription: 'Settings.Lists.Nationalities.LongDescription',
          tableConfiguration: NationalitiesSourcesConfig
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Nationalities',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Nationalities',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/MediaTypes',
        component: AdvancedListComponent,
        data: {
          model: 'MediaTypes',
          list: 'active',
          description: 'Settings.Lists.MediaType.Description',
          longDescription: 'Settings.Lists.MediaType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'MediaTypes',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'MediaTypes',
              action: 'newEdit',
              closeOnSubmit: true,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/:model',
        component: ListComponent,
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              model: AdvancedFormModelResolver,
              formConfig: EnumerationModelFormResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true
            },
            resolve: {
              model: AdvancedFormModelResolver,
              data: AdvancedFormItemResolver,
              formConfig: EnumerationModelFormResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SettingsRoutingModule {
}
