import {Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RouterModalPreviousNextCancel} from '@universis/common/routing';
import {combineLatest, forkJoin, Subscription} from 'rxjs';
import {BsLocaleService, TabsetComponent} from 'ngx-bootstrap';
import {ConfigurationService, ErrorService, LoadingService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Form, NgForm} from '@angular/forms';
import {AngularDataContext} from '@themost/angular';
import {DatePipe} from '@angular/common';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import {ResponseError} from '@themost/client';

@Component({
  selector: 'app-enrollment-event-edit',
  templateUrl: './enrollment-event-edit.component.html',
  styles: [`
      .tab-wizard .nav {
          display: none;
        }
      .tab-wizard .tab-content {
          border-top: none !important;
        }
      .simple-check-list {
        height: 164px;
        overflow-y: auto;
      }
      .form-required::after {
        content: "(*)"
      }
      .form-control[readonly] {
        background-color: #fff;
      }
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class EnrollmentEventEditComponent extends RouterModalPreviousNextCancel  implements OnInit, OnDestroy {

  public model: any = {};
  @Input('department') department: any;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  private paramsSubscription: Subscription;
  public currentTab = 0;
  @ViewChild('form1') form1: NgForm;
  @ViewChild('form2') form2: NgForm;
  @ViewChild('form3') form3: NgForm;
  public activeForm: NgForm;
  public lastError: any;
  private formStatusSubscription: Subscription;
  public attachmentTypes: Array<any> = [];
  public studyPrograms: Array<any> = [];
  public inscriptionModes: Array<any> = [];
  public articles: Array<any> = [];

  compareSelectOption(a, b) {
    // tslint:disable-next-line:triple-equals
    return (a == b) || (a && b && (a.id == b.id || a == b.id || a.id == b));
  }

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _configurationService: ConfigurationService,
              private _localeService: BsLocaleService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Candidates.EditEvent';
  }

  cancel(): Promise<any> {
    return this.close();
  }

  setActiveForm(current: number) {
    // set active form
    switch (current) {
      case 0: this.activeForm = this.form1; break;
      case 1: this.activeForm = this.form2; break;
      case 2: this.activeForm = this.form3; break;

    }
    if (this.activeForm) {
      if (this.formStatusSubscription) {
        this.formStatusSubscription.unsubscribe();
      }
      this.formStatusSubscription = this.activeForm.statusChanges.subscribe(() => {
        // validate form status
        this.nextButtonDisabled = this.activeForm.invalid;
      });
      // initial state
      this.nextButtonDisabled = this.activeForm.invalid;
    }
  }

  async previous(): Promise<any> {
    // clear error
    this.lastError = null;
    if (this.currentTab > 0) {
      this.currentTab -= 1;
      this.staticTabs.tabs[this.currentTab].active = true;
      this.previousButtonDisabled = (this.currentTab === 0);
      this.nextButtonText = this._translateService.instant('Forms.Next') + ' >';
      this.setActiveForm(this.currentTab);
      return;
    }
  }

  async next(): Promise<any> {
    // clear error
    this.lastError = null;
    if (this.currentTab < this.staticTabs.tabs.length - 1) {
      this.currentTab += 1;
      this.staticTabs.tabs[this.currentTab].active = true;
      this.previousButtonDisabled = false;
      if (this.currentTab === this.staticTabs.tabs.length - 1) {
        this.nextButtonText = this._translateService.instant('Forms.Submit');
      }
      this.setActiveForm(this.currentTab);
      return;
    }
    // this is the last tab so submit form
    if (this.currentTab === this.staticTabs.tabs.length - 1) {
      const result = await this.submit();
      if (result) {
        return this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }
    }
  }

  /**
   * Sync selected inscription modes before submit
   */
  private beforeSubmitInscriptionModes() {
    // get selected items
    const selectedItems = this.inscriptionModes.filter((x) => x.selected);
    this.model.inscriptionModes = this.model.inscriptionModes || [];
    // try to remove items (set item state to be removed)
    this.model.inscriptionModes.forEach((inscriptionMode) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === inscriptionMode.id);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(inscriptionMode, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((inscriptionMode) => {
      const findIndex = this.model.inscriptionModes.findIndex((x) => x.id === inscriptionMode.id);
      if (findIndex < 0) {
        // add item
        this.model.inscriptionModes.push(inscriptionMode);
      }
    });
    // ensure default inscription mode is also in allowed inscription modes
    if (this.model.inscriptionMode) {
      const defaultInscriptionMode = this.model.inscriptionModes.find((inscriptionMode) => {
        return inscriptionMode.id === this.model.inscriptionMode.id;
      });
      if (defaultInscriptionMode && defaultInscriptionMode.$state === 4) {
        // delete state
        delete defaultInscriptionMode.$state;
      } else if (defaultInscriptionMode == null) {
        // else, push default mode
        this.model.inscriptionModes.push(this.model.inscriptionMode);
      }
    }
  }

  /**
   * Sync selected inscription modes before submit
   */
  private beforeSubmitArticles() {
    // get selected items
    const selectedItems = this.articles.filter((x) => x.selected);
    this.model.articles = this.model.articles || [];
    // try to remove items (set item state to be removed)
    this.model.articles.forEach((article) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === article.id);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(article, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((article) => {
      const findIndex = this.model.articles.findIndex((x) => x.id === article.id);
      if (findIndex < 0) {
        // add item
        this.model.articles.push(article);
      }
    });
  }

  /**
   * Sync selected attachment types before submit
   */
  private beforeSubmitAttachmentTypes() {
    // get selected items
    const selectedItems = this.attachmentTypes.filter((x) => x.selected);
    this.model.attachmentTypes = this.model.attachmentTypes || [];
    // try to remove items (set item state to be removed)
    this.model.attachmentTypes.forEach((attachmentConfiguration) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === attachmentConfiguration.attachmentType);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(attachmentConfiguration, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((attachmentType) => {
      const findIndex = this.model.attachmentTypes.findIndex((x) => x.attachmentType === attachmentType.id);
      if (findIndex < 0) {
        // add item
        this.model.attachmentTypes.push({
          attachmentType: attachmentType
        });
      }
    });
  }



  async submit() {
    try {
      this._loadingService.showLoading();
      // check start date
        if (this.model.validThrough && this.model.validFrom) {
          if (this.model.validThrough < this.model.validFrom) {
            throw new Error(this._translateService.instant('Candidates.CheckDates'));
          }
        } else {
          throw new Error(this._translateService.instant('Candidates.CheckDates'));
        }

      // sync attachment types
      this.beforeSubmitAttachmentTypes();
        // sync inscription modes
      this.beforeSubmitInscriptionModes();
      // sync articles
      this.beforeSubmitArticles();
      // and finally submit
      await this._context.model('StudyProgramEnrollmentEvents').save(this.model);
      this._loadingService.hideLoading();
      return true;
    } catch (err) {
      this._loadingService.hideLoading();
      this.lastError = err;
      return false;
    }
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.formStatusSubscription) {
      this.formStatusSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.nextButtonDisabled = false;
    this.setActiveForm(this.currentTab);
    this.nextButtonText = this._translateService.instant('Forms.Next') + ' >';
    this.previousButtonDisabled = true;
    this.previousButtonText = '< ' + this._translateService.instant('Forms.Previous');

    this._loadingService.showLoading();
    this.paramsSubscription = combineLatest([
      this.activatedRoute.data,
      this.activatedRoute.params
    ]).subscribe((data) => {
      const routeData = data[0];
      const routeParams = data[1];
      // get action
      if (routeData.action === 'new') {
        // set modal title
        this.modalTitle = 'Candidates.New';
      }
      // get active department
      this._activeDepartmentService.getActiveDepartment().then((activeDepartment) => {
        // prepare study programs base query
        const studyProgramQueryBase = this._context.model('StudyPrograms')
          .where('department').equal(activeDepartment.id)
          .select('id', 'name', 'studyLevel/name as studyLevel', 'currentYear', 'currentPeriod')
          .take(-1)
          .orderBy('name');
        // get academic years, periods
        const sources = [
          this._context.model('AttachmentTypes').asQueryable()
              .take(-1)
              .orderBy('name')
              .getItems(),
          routeData.action === 'new' ?
            // append isActive property to query
            studyProgramQueryBase.and('isActive').equal(true).getItems()
              : studyProgramQueryBase.getItems(),
          this._context.model('InscriptionModes')
            .select('id', 'name')
            .take(-1)
            .orderBy('name')
            .getItems(),
          this._context.model('ActionStatusArticles')
            .select('id', 'name', 'inLanguage', 'actionStatus')
            .expand('actionStatus')
            .take(-1)
            .orderBy('actionStatus,inLanguage')
            .getItems(),
        ];
        if (routeParams.id) {
          sources.push(
              this._context.model('StudyProgramEnrollmentEvents')
                  .where('id').equal(routeParams.id)
                .expand('studyProgram, inscriptionMode, attachmentTypes, inscriptionModes, articles')
                  .getItem()
          );
        } else {
          sources.push(Promise.resolve({
            organizer: activeDepartment,
            inscriptionYear: activeDepartment && activeDepartment.currentYear,
            inscriptionPeriod: activeDepartment && activeDepartment.currentPeriod
          }));
        }
        // get data
        return Promise.all(sources).then((results) => {
          this.attachmentTypes = results[0];
          this.studyPrograms = results[1];
          this.inscriptionModes = results[2];
          this.articles = results[3];
          this.model = results[4];
          if (this.model == null) {
            throw new ResponseError('Not Found', 404);
          }
          // select study programs
          if (Array.isArray(this.model.studyPrograms)) {
            this.studyPrograms.forEach((studyProgram) => {
              const findIndex = this.model.studyPrograms.findIndex((x) => x.id === studyProgram.id);
              studyProgram.selected = findIndex >= 0;
            });
          }
          // select inscription modes
          if (Array.isArray(this.model.inscriptionModes)) {
            this.inscriptionModes.forEach((inscriptionMode) => {
              const findIndex = this.model.inscriptionModes.findIndex((x) => x.id === inscriptionMode.id);
              inscriptionMode.selected = findIndex >= 0;
            });
          }
          // select attachment types
          if (Array.isArray(this.model.attachmentTypes)) {
            this.attachmentTypes.forEach((attachmentType) => {
              const findIndex = this.model.attachmentTypes.findIndex((x) => x.attachmentType === attachmentType.id);
              attachmentType.selected = findIndex >= 0;
            });
          }
          // select inscription modes
          if (Array.isArray(this.model.articles)) {
            this.articles.forEach((article) => {
              const findIndex = this.model.articles.findIndex((x) => x.id === article.id);
              article.selected = findIndex >= 0;
            });
          }
          this._loadingService.hideLoading();
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    });
  }


}
