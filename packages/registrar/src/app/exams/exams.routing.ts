import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';
import { ExamsPreviewInstructorsComponent } from './components/exams-preview-instructors/exams-preview-instructors.component';
import { ExamsPreviewGradingComponent } from './components/exams-preview-grading/exams-preview-grading.component';
import { ExamsPreviewGradesCheckComponent } from './components/exams-preview-grades-check/exams-preview-grades-check.component';
import { AdvancedFormRouterComponent } from '@universis/forms';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData} from '@universis/forms';
import {ExamsAddInstructorComponent} from './components/exams-preview-instructors/exams-add-instructor.component';
import { CurrentAcademicYearResolver } from '../registrar-shared/services/activeDepartmentService.service';
import {ExamsPreviewTestTypesComponent} from './components/exams-preview-test-types/exams-preview-test-types.component';
import {ExamsAddTestTypeComponent} from './components/exams-preview-test-types/exams-add-test-type.component';
import {ExamsSharedModule} from './exams.shared';
import {SelectReportComponent} from '../reports-shared/components/select-report/select-report.component';
import { ExamsPreviewParticipationsComponent } from './components/exams-preview-participations/exams-preview-participations.component';
// tslint:disable-next-line:max-line-length
import { ExamsNewGradeSubmissionComponent } from './components/exams-new-grade-submission/exams-new-grade-submission.component';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';

const routes: Routes = [
  // {
  //   path: '',
  //   component: ExamsHomeComponent,
  //   data: {
  //     title: 'Exams'
  //   },
  //   children: [
  //     {
  //       path: '',
  //       pathMatch: 'full',
  //       redirectTo: 'list/currentYear'
  //     },
      {
        path: '',
        component: ExamsTableComponent,
        data: {
          model: 'CourseExams',
          title: 'Exams List'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver
        }
    //   }
    // ]
  },
  {
    path: ':id',
    component: ExamsRootComponent,
    data: {
      title: 'Exam Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: ExamsPreviewComponent,
        data: {
          title: 'Exam Preview'
        },
        children: [
          {
            path: '',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: ExamsPreviewGeneralComponent,
            data: {
              title: 'Exams Preview General'
            },
            children: [
              {
                path: 'print',
                pathMatch: 'full',
                component: SelectReportComponent,
                outlet: 'modal',
                resolve: {
                  item: AdvancedFormItemResolver
                }
              }
          ]
          },
          {
            path: 'students',
            component: ExamsPreviewStudentsComponent,
            data: {
              title: 'Exams Preview Students',
              model: 'CourseExamStudentGrades'
            },
            resolve: {
              searchConfiguration: SearchConfigurationResolver,
              tableConfiguration: TableConfigurationResolver
            }
          },
          {
            path: 'examsParticipations',
            component: ExamsPreviewParticipationsComponent,
            data: {
              title: 'Exams Participations',
              model: 'CourseExamParticipateActions'
            },
            resolve: {
              searchConfiguration: TableConfigurationResolver,
              tableConfiguration: TableConfigurationResolver
            }
          },
          {
            path: 'instructors',
            component: ExamsPreviewInstructorsComponent,
            data: {
              title: 'Course Exam Instructors',
              model: 'CourseExamInstructors'
            },
            resolve: {
              tableConfiguration: TableConfigurationResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: ExamsAddInstructorComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Exams.AddInstructor',
                  model: 'CourseExams'
                },
                resolve: {
                  courseExam: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'testTypes',
            component: ExamsPreviewTestTypesComponent,
            data: {
              title: 'Exams Preview Test Types',
              model: 'CourseExamTestTypes'
            },
            resolve: {
              tableConfiguration: TableConfigurationResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: ExamsAddTestTypeComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Exams.AddTestType',
                  model: 'CourseExams'
                },
                resolve: {
                  courseExam: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'grading',
            component: ExamsPreviewGradingComponent,
            data: {
              title: 'Course exam upload actions',
              model: 'ExamDocumentUploadActions',
              list: 'examActions'
            },
            resolve: {
              tableConfiguration: TableConfigurationResolver
            }
          },
          {
            path: 'grading/new',
            component: ExamsNewGradeSubmissionComponent,
            data: {
              title: 'Exams New Grade Submission'
            }
          },
          {
            path: 'grading/:id',
            component: ExamsPreviewGradesCheckComponent,
            data: {
              title: 'Exams Preview Display Grades'
            }
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent,
        data: {
          title: 'Exams.Edit',
          model: 'CourseExams', 
          serviceParams: {
            $expand: 'course,examPeriod,status,completedByUser,year'
          }, 
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class ExamsRoutingModule {
}
