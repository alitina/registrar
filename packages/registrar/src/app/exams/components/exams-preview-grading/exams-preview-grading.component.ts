import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration,
  AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription, combineLatest } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-exams-preview-grading',
  templateUrl: './exams-preview-grading.component.html',
  styleUrls: []
})

export class ExamsPreviewGradingComponent implements OnInit, OnDestroy {

  @ViewChild('actions') actions: AdvancedTableComponent;
  courseExamID: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  public studyProgram: number;
  public specialization: number;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _templateManageService: TemplateManageService
  ) {
  }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    this.subscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data).subscribe(async (results) => {
      const params = results[0];
      const data = results[1];
      this.specialization = params.specialization;
      this._activatedTable.activeTable = this.actions;
      this.actions.query = this._context.model(`CourseExams/${params.id}/actions`)
        .asQueryable()
        .where('actionStatus/alternateName').equal('CompletedActionStatus')
        .or('actionStatus/alternateName').equal('FailedActionStatus')
        .or('actionStatus/alternateName').equal('ActiveActionStatus')
        .prepare().and('additionalResult').notEqual(null)
         .expand('owner,grades($select=action,count(id) as totalCount;$groupby=action)')
        .orderBy('dateCreated')
        .prepare();

      this.actions.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
      this.actions.fetch();

    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(true);
  }
}
