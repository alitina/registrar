import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription, combineLatest } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-exams-preview-instructors',
  templateUrl: './exams-preview-instructors.component.html',
  styleUrls: []
})

export class ExamsPreviewInstructorsComponent implements OnInit, OnDestroy {

  @ViewChild('instructors') instructors: AdvancedTableComponent;
  courseExamID: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    private _templateManageService: TemplateManageService
  ) { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
    this.subscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data).subscribe(async (results) => {
      const params = results[0];
      const data = results[1];
      this._activatedTable.activeTable = this.instructors;
      this.instructors.query = this._context.model(`CourseExams/${params.id}/instructors`)
        .asQueryable()
        .expand('instructor')
        .prepare();

      this.instructors.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
      this.instructors.fetch();


      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.instructors.fetch(true);
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this._templateManageService.updateShowStatus(false)
  }

  remove() {
    if (this.instructors && this.instructors.selected && this.instructors.selected.length) {
      // get items to remove
      const items = this.instructors.selected.map(item => {
        return {
          instructor: item.id,
          courseExam: this.courseExamID
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Exams.RemoveInstructorTitle'),
        this._translateService.instant('Exams.RemoveInstructorMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('courseExamInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Exams.RemoveInstructorsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Exams.RemoveInstructorsMessage.one' : 'Exams.RemoveInstructorsMessage.many')
                  , { value: items.length })
              );
              this.instructors.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}
