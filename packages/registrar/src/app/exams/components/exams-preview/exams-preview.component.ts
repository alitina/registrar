import { Component, OnDestroy, OnInit } from '@angular/core';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-exams-preview',
  templateUrl: './exams-preview.component.html',
  styleUrls: ['./exams-preview.component.scss']
})
export class ExamsPreviewComponent implements OnInit, OnDestroy {

  constructor(
    private _templateManageService: TemplateManageService)  { }

  async ngOnInit() {
    this._templateManageService.updateShowStatus(false);
   }

  ngOnDestroy(): void {
    this._templateManageService.updateShowStatus(true);
  }

}
