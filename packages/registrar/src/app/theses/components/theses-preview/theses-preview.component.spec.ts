import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesPreviewComponent } from './theses-preview.component';

describe('ThesesPreviewComponent', () => {
  let component: ThesesPreviewComponent;
  let fixture: ComponentFixture<ThesesPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
