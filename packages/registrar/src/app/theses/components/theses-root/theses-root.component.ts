import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';
import {TableConfiguration} from '@universis/ngx-tables';
import {Subscription} from 'rxjs';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
  TemplatePipe,
  ToastService,
  UserActivityService
} from '@universis/common';

@Component({
  selector: 'app-theses-root',
  templateUrl: './theses-root.component.html',
})
export class ThesesRootComponent implements OnInit, OnDestroy {
  public model: any;
  public isCreate = false;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private changeSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _template: TemplatePipe,
              private _router: Router,
              private _appEvent: AppEventService,
              private _toastService: ToastService
  ) { }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      const thesisId = params.id;
      await this.load(thesisId);
    });
    this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
      if (event && (event.model === 'Theses') && event.target ) {
        // reload
        await this.load(event.target);
      }
    });
  }

  private async load(thesisId) {
    this.model = await this._context.model('Theses')
      .where('id').equal(thesisId)
      .expand('instructor,type,status,students($expand=student($expand=department,person))')
      .getItem();

    if (this.model) {
      if (this.model.students.length > 0) {
        await this._userActivityService.setItem({
          category: this._translateService.instant('Theses.Title'),
          description: this._translateService.instant(this.model.students[0].student.familyName +
            ' ' + this.model.students[0].student.givenName),
          url: '/theses/' + thesisId + '/dashboard',
          dateCreated: new Date
        });
      }

      // @ts-ignore
      this.config = cloneDeep(THESES_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.model) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action' || x.role === 'method') {
              if (x.access && x.access.length > 0) {
                let access = x.access;
                access = access.filter(y => {
                  if (y.thesis) {
                    x.disabled = !y.thesis.find(z => z.status === this.model.status.alternateName);
                  }
                  return y;
                });
                if (access && access.length > 0) {
                  return x;
                }
              } else {
                return x;
              }
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = template(x.href)(this.model);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          if (action.role === 'action') {
            action.href = this._template.transform(action.href, this.model);
          } else if (action.role === 'method') {
            action.click = () => {
              if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
            };
          }
        });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  /**
   * Deletes selected Theses
   */
  async deleteAction() {
    if (this.model && this.model.students && this.model.students.length === 0) {
      return this._modalService.showWarningDialog(
        this._translateService.instant('Theses.DeleteAction.Title'),
        this._translateService.instant('Theses.DeleteAction.Description'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._loadingService.showLoading();
          this._context.model('Theses').remove(this.model).then(() => {
            this._toastService.show(
              this._translateService.instant('Theses.DeleteAction.Completed.Title'),
              this._translateService.instant('Theses.DeleteAction.Completed.Description')
            );
            this._loadingService.hideLoading();
            // reload url
            this._router.navigate(['/theses']);
          }).catch((err) => {
            // stop progress
            this._loadingService.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    } else {
      const errorTitle = this._translateService.instant('Theses.DeleteAction.CannotDelete.Title');
      const errorMsg = this._translateService.instant('Theses.DeleteAction.CannotDelete.Description');
      return this._modalService.showWarningDialog(errorTitle, errorMsg);
    }
  }
}
