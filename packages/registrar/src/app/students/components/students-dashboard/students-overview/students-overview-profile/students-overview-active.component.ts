import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-students-overview-active',
    templateUrl: './students-overview-active.component.html'
})
export class StudentsOverviewActiveComponent implements OnInit, OnDestroy  {

    public student;
    @Input() studentId: number;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }


  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('department,user,studyProgramSpecialty, person($expand=gender),studyProgram($expand=locale)')
        .getItem()
        .then((value) => { this.student = value; });
    }
  }
    async ngOnInit() {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.student = await this._context.model('Students')
          .where('id').equal(this.studentId)
          .expand('department,user, studyProgram, studyProgramSpecialty,inscriptionPeriod($expand=locale),person($expand=gender),studyProgram($expand=locale)')
          .getItem();
      });
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
        if (fragment && fragment === 'reload') {
          this.student = await this._context.model('Students')
            .where('id').equal(this.studentId)
            .expand('department,user,studyProgramSpecialty, person($expand=gender),studyProgram($expand=locale)')
            .getItem();
        }
      });
    }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
