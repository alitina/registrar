import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { UserService, ErrorService, ModalService, AppEventService } from '@universis/common';
import { Subscription } from 'rxjs';
import { AddAttachmentComponent } from './add-attachment/add-attachment.component';

@Component({
  selector: 'app-students-attachments',
  templateUrl: './students-attachments.component.html',
  styleUrls: ['./students-attachments.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StudentsAttachmentsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private eventSubscription: Subscription;
  public model: any;
  public currentUser: any;
  public studentId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _userService: UserService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _appEvent: AppEventService
  ) {}

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(
      async (params) => {
        // get student id
        this.studentId = params.id;
        this.model = await this.fetchAttachments();
        // get current user
        const user = await this._userService.getUser();
        this.currentUser = user && user.id;
        this.eventSubscription = this._appEvent.changed.subscribe(change => {
          if (change && change.model === 'AddStudentAttachment') {
            this.fetchAttachments().then((attachments) => {
              this.model = attachments;
            });
          }
        });
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
  }

  preview(attachment: any) {
    return this._router.navigate(['.'], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: true,
      queryParams: {
        download: attachment.url,
      },
    });
  }

  download(attachment: any) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include',
    })
      .then((response) => {
        return response.blob();
      })
      .then((blob) => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  async remove(attachment: any) {
    try {
      // remove attachment
      await this._context.model(`Students/${this.studentId}/attachments/${attachment.id}/remove`).save(null);
      // refresh data
      this.model = await this.fetchAttachments();
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async fetchAttachments() {
      // get student attachments
      return this._context
      .model(`Students/${this.studentId}/attachments`)
      .asQueryable()
      .expand('attachmentType, createdBy')
      .getItems();
  }

  uploadAttachment() {
    try {
      this._modalService.openModalComponent(AddAttachmentComponent, {
        class: 'modal-lg',
        ignoreBackdropClick: true,
        closeOnSubmit: true,
        initialState: {
          data: {
            studentId: this.studentId
          }
        }
      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
}
