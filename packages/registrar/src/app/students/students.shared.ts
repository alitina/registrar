// tslint:disable:max-line-length
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {StudentsOverviewCoursesComponent} from './components/students-dashboard/students-overview/students-overview-courses/students-overview-courses.component';
import {StudentsOverviewLastRegistrationComponent} from './components/students-dashboard/students-overview/students-overview-lastregistration/students-overview-last-registration.component';
import {StudentsOverviewCurrentgradesComponent} from './components/students-dashboard/students-overview/students-overview-currentgrades/students-overview-currentgrades.component';
import {StudentsOverviewRequestsComponent} from './components/students-dashboard/students-overview/students-overview-requests/students-overview-requests.component';
import {StudentsOverviewInternshipsComponent} from './components/students-dashboard/students-overview/students-overview-internships/students-overview-internships.component';
import {TooltipModule} from 'ngx-bootstrap';
import {StudentsOverviewScholarshipsComponent} from './components/students-dashboard/students-overview/students-overview-scholarships/students-overview-scholarships.component';
import {StudentsOverviewThesesComponent} from './components/students-dashboard/students-overview/students-overview-theses/students-overview-theses.component';
import { StudentsOverviewProfileComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-profile.component';
import {FormsModule} from '@angular/forms';
import {StudentsPreviewFormComponent} from './components/students-dashboard/students-general/students-preview-form.component';
import { StudentsMessagesPreviewFormComponent } from './components/students-dashboard/students-messages-preview-form/students-messages-preview-form.component';
import { StudentsStatsComponent } from './components/students-dashboard/students-courses/students-stats/students-stats.component';
import { MessagesSharedModule } from '../messages/messages.shared';
import { StudentsOverviewGraduationComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-graduation.component';
import { StudentsOverviewActiveComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-active.component';
import { StudentsOverviewRemovalComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-removal.component';
import * as STUDENTS_COURSE_EXEMPTION from './components/students-dashboard/students-courses/students-courses-exemption/students-courses-exemption-table.config.list.json';

import {StudentsThesesConfigurationResolver, StudentsDefaultThesesConfigurationResolver} from './components/students-dashboard/students-theses/students-theses-config.resolver';
import {StudentsCoursesExemptionComponent} from './components/students-dashboard/students-courses/students-courses-exemption/students-courses-exemption.component';
import {CoursesSharedModule} from '../courses/courses.shared';
import {AdvancedFormsModule} from '@universis/forms';
import {RouterModalModule} from '@universis/common/routing';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {SelectCourseComponent} from '../courses/components/select-course/select-course-component';
import {GradePipe, SharedModule, GradeScale, ServerEventService, AppEventService} from '@universis/common';
import {StudentsOverviewSnapshotComponent} from './components/students-dashboard/students-overview/students-overview-profile/students-overview-snapshot.component';
import {
  StudentsInformationsConfigurationResolver
} from './components/students-dashboard/students-information/students-informations-config.resolver';
import { AddAttachmentComponent } from './components/students-dashboard/students-attachments/add-attachment/add-attachment.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { RouterModule } from '@angular/router';
import { StudentsCoursesPercentagesEditComponent } from './components/students-dashboard/students-courses/students-courses-percentages-edit/students-courses-percentages-edit.component';
import { StudentsCourseEditComponent } from './components/students-dashboard/students-courses/students-course-edit/students-course-edit.component';
import { StudyProgramsSharedModule } from '../study-programs/study-programs.shared';

@NgModule({
    imports: [
        AdvancedFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule,
        TooltipModule.forRoot(),
        FormsModule,
        MessagesSharedModule,
        CoursesSharedModule,
        RouterModalModule,
        AdvancedFormsModule,
        SettingsSharedModule,
        SharedModule,
        NgxDropzoneModule,
        PdfViewerModule,
        StudyProgramsSharedModule

    ],
  declarations: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent,
    StudentsOverviewGraduationComponent,
    StudentsOverviewActiveComponent,
    StudentsOverviewRemovalComponent,
    StudentsCoursesExemptionComponent,
    StudentsOverviewSnapshotComponent,
    AddAttachmentComponent,
    StudentsCoursesPercentagesEditComponent,
    StudentsCourseEditComponent
  ],
  exports: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent,
    StudentsOverviewGraduationComponent,
    StudentsOverviewActiveComponent,
    StudentsOverviewRemovalComponent,
    StudentsOverviewSnapshotComponent,
    AddAttachmentComponent
  ],
  providers: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsStatsComponent,
    StudentsOverviewProfileComponent,
    StudentsThesesConfigurationResolver,
    StudentsDefaultThesesConfigurationResolver,
    GradePipe,
    StudentsInformationsConfigurationResolver
  ],
  entryComponents: [
    StudentsCoursesExemptionComponent,
    SelectCourseComponent,
    AddAttachmentComponent,
    StudentsOverviewProfileComponent,
    StudentsCoursesPercentagesEditComponent,
    StudentsCourseEditComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class StudentsSharedModule {
  public  static readonly  StudentsStudyProgramCoursesList = STUDENTS_COURSE_EXEMPTION;
  

  constructor(private _translateService: TranslateService, private serverEvent: ServerEventService, private appEvent: AppEventService) {
    const sources = environment.languages.map((language: string) => {
      return import(`./i18n/students.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
        return Promise.resolve();
      })
    });
    Promise.all(sources).then(() => {
      // emit event
      this.appEvent.add.next({
        service: this._translateService,
        type: this._translateService.setTranslation
      });
    }).catch((err) => {
      console.error('An error occurred while loading shared module');
      console.error(err);
    });
  }

}
