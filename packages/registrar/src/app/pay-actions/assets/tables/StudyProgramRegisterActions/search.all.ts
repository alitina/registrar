export const StudyProgramRegisteActionPayActions_Search_All = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Students.studyProgram",
              "labelPosition": "top",
              "widget": "choicesjs",
              "key": "studyProgram",
              "dataSrc": "url",
              "data": {
                "url": "StudyPrograms?$top=-1&$skip=0&$filter=department eq '{{form.department}}'",
                "headers": []
              },
              "template": "{{ item.name }}",
              "selectValues": "value",
              "valueProperty": "id",
              "type": "select",
              "input": true,
              "disabled": false,
              "lazyLoad": false,
              "searchEnabled": true
            }

          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Register.Candidate",
              "placeholder": "Register.SearchCandidate",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "candidateName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Register.CandidateNumber",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "inscriptionNumber",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.Preview.Status",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "ActionStatusTypes",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.alternateName }}",
              "key": "actionStatus",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.inscriptionYear",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "AcademicYears?$orderby=id desc",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.name }}",
              "key": "inscriptionYear",
              "valueProperty": "id",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.inscriptionPeriod",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "AcademicPeriods?$expand=locale&orderby=id",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
              },
              "template": "{{ item.locale.name }}",
              "key": "inscriptionPeriod",
              "valueProperty": "id",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false,
              "searchEnabled": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.InscriptionMode",
              "labelPosition": "top",
              "key": "inscriptionMode",
              "type": "select",
              "searchEnabled": true,
              "valueProperty": "id",
              "selectValues": "value",
              "dataSrc": "url",
              "data": {
                "url": "InscriptionModes?$top=-1&$orderby=name",
                "headers": []
              },
              "template": "{{ item.name }}",
              "input": true,
              "lazyLoad": true,
              "widget": "choicesjs"
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        }
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
