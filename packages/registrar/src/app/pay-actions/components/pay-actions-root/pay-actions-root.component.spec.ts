import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PayActionsRootComponent } from './pay-actions-root.component';


describe('PayActionsRootComponent', () => {
  let component: PayActionsRootComponent;
  let fixture: ComponentFixture<PayActionsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayActionsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayActionsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
