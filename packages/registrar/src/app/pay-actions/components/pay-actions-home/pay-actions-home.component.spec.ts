import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayActionsHomeComponent } from './pay-actions-home.component';

describe('PayActionsHomeComponent', () => {
  let component: PayActionsHomeComponent;
  let fixture: ComponentFixture<PayActionsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayActionsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayActionsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
