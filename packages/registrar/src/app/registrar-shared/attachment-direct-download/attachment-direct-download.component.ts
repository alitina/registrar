import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ResponseError } from '@themost/client';
import { ErrorService, LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-attachment-direct-download',
  templateUrl: './attachment-direct-download.component.html',
  styleUrls: ['./attachment-direct-download.component.scss'],
})
export class AttachmentDirectDownloadComponent implements OnInit, OnDestroy {
  private queryParamsSubscription: Subscription;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _router: Router,
    private _loadingService: LoadingService
  ) {}

  ngOnInit() {
    this.queryParamsSubscription = this._activatedRoute.queryParams.subscribe(
      async (queryParams) => {
        if (queryParams.download) {
            try {
                this._loadingService.showLoading();
                await this.download(
                    queryParams.download,
                    queryParams.name,
                    queryParams.type
                  );
            } catch (err) {
                this._errorService.showError(err, {
                    continueLink: '.'
                });
            } finally {
              this._loadingService.hideLoading();
              // reset query params
              this._router.navigate([], {queryParams: null});
            }
        }
      }
    );
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(
    attachmentURL: string,
    attachmentName: string = 'attachment.pdf',
    attachmentType: string = 'application/pdf'
  ) {
    const headers = new Headers({
      Accept: attachmentType,
    });
    const url = attachmentURL;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context
      .getService()
      .resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include',
    })
      .then((response) => {
        if (response.ok === false) {
          throw new ResponseError(response.statusText, response.status);
        }
        return response.blob();
      })
      .then((blob) => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = attachmentName;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  ngOnDestroy(): void {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
  }
}
