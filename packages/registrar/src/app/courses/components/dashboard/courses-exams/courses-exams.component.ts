import { Component, OnInit, Input, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription, combineLatest } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ErrorService, ModalService, ToastService, DIALOG_BUTTONS } from '@universis/common';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';
import { TemplateManageService } from '@longis/ngx-longis/registrar';

@Component({
  selector: 'app-courses-exams',
  templateUrl: './courses-exams.component.html',
  styleUrls: []
})

export class CoursesExamsComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  private subscription: Subscription;
  private currentStudyProgramSubscription: Subscription;

  @ViewChild('exams') exams: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  public courseId;
  private studyProgram: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext,
    private _tableResolver: TableConfigurationResolver,
    private _searchResolver: SearchConfigurationResolver,
    private _templateManageService: TemplateManageService) { }

  async ngOnInit() {

    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._tableResolver.get('CourseExams', 'courseExams'),
      this._searchResolver.get('CourseExams', 'courseExams')
    ).pipe(
      map(([params, tableConfiguration, searchConfiguration]) => ({ params, tableConfiguration, searchConfiguration }))
    ).subscribe(async (results) => {
      this._activatedTable.activeTable = this.exams;
      this.courseId = results.params.id;
      this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);

      this.exams.query = this._context.model(`StudyPrograms/${this.studyProgram}/exams`)
        .where('course').equal(results.params.id)
        .prepare();

      this.exams.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      this.exams.fetch();

      if (results.searchConfiguration) {
        this.search.form = results.searchConfiguration;
        this.search.ngOnInit();
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.exams.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.exams && this.exams.selected && this.exams.selected.length) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.courseId = params.id;
        const items = this.exams.selected.map(item => {
          return {
            course: this.courseId,
            id: item.id
          };
        });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Courses.RemoveExamsTitle'),
        this._translateService.instant('Courses.RemoveExamMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseExams').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Courses.RemoveExamsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Courses.RemoveExamsMessage.one' : 'Courses.RemoveExamsMessage.many')
                  , { value: items.length })
              );
              this.exams.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
      });
    }
  }

}
