import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { TemplateManageService } from '@longis/ngx-longis/registrar';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-classes',
  templateUrl: './courses-overview-classes.component.html',
  styleUrls: ['./courses-overview-classes.component.scss']
})
export class CoursesOverviewClassesComponent implements OnInit, OnDestroy {
  @Input() currentYear: any;
  public courseId: any;
  public model: any;
  public currentCourseClasses: any;
  private currentStudyProgramSubscription: Subscription;
  private paramSubscription: Subscription;
  public studyProgram: number;
  public specialization: number;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext, 
              private _templateManageService: TemplateManageService) {
}


  async ngOnInit() {
    this.currentStudyProgramSubscription = this._templateManageService.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram);
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      if (params && params.specialization) {
        this.specialization = params.specialization;
        this.courseId = params.id;
      }
    });
    const studyProgramEntity = await this._context.model('StudyPrograms')
                      .where('id').equal(this.studyProgram)
                      .getItem();
    this.currentCourseClasses = await this._context.model(`StudyPrograms/${this.studyProgram}/classes`)
                      .where('course').equal(this.courseId)
                      .and('year').equal(studyProgramEntity.currentYear.id)
                      .and('period').equal(studyProgramEntity.currentPeriod.id)
                      .expand('status,instructors($expand=instructor),course($expand=department),period($expand=locale),students($select=courseClass,count(id) as total;$groupby=courseClass)')
                      .getItems();
  }

  ngOnDestroy(): void {
    if (this.currentStudyProgramSubscription) {
      this.currentStudyProgramSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
