import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesTableComponent} from './components/courses-table/courses-table.component';
import {CoursesRootComponent} from './components/courses-root/courses-root.component';
import {CoursesPreviewGeneralComponent} from './components/dashboard/courses-preview-general/courses-preview-general.component';
import {
  CoursesPreviewClassesComponent,
  CourseTitleResolver
} from './components/dashboard/courses-preview-classes/courses-preview-classes.component';
import {CoursesOverviewComponent} from './components/dashboard/courses-overview/courses-overview.component';
import {CoursesExamsComponent} from './components/dashboard/courses-exams/courses-exams.component';
import {CoursesStudyProgramsComponent} from './components/dashboard/courses-study-programs/courses-study-programs.component';
// tslint:disable-next-line:max-line-length
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '@universis/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData,
  AdvancedFormParentItemResolver
} from '@universis/forms';
import {
  ActiveDepartmentIDResolver, ActiveDepartmentResolver, CurrentAcademicPeriodResolver, CurrentAcademicYearResolver, LastStudyProgramResolver
} from '../registrar-shared/services/activeDepartmentService.service';
// tslint:disable-next-line:max-line-length
import {AdvancedListComponent} from '@universis/ngx-tables';
import * as GradeScaleListConfig from './gradeScales.config.list.json';
import {CoursesStudentsComponent} from './components/dashboard/courses-students/courses-students.component';
import {CourseUpdateResultsComponent} from './components/dashboard/course-update-results/course-update-results.component';
import { ChangeCourseGradeScaleComponent } from './components/dashboard/courses-overview/change-course-grade-scale/change-course-grade-scale/change-course-grade-scale.component';
import { GradeScaleWithLocalesResolver } from './gradeScale.resolver';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';


const routes: Routes = [
  {
    path: 'configuration/GradeScales',
    component: AdvancedListComponent,
    data: {
      model: 'GradeScales',
      tableConfiguration: GradeScaleListConfig,
      description: 'Settings.Lists.GradeScale.Description',
      longDescription: 'Settings.Lists.GradeScale.LongDescription',
      category: 'Settings.Title'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true,
          serviceQueryParams: {
            $expand: 'locales,values($expand=locales)'
          }
        },
        resolve: {
          data: GradeScaleWithLocalesResolver
        }
      }
    ]
  },
  {
    // /#/study-programs/256/preview/specialization/-1/courses/list/activeCourse
    path: '',
    component: CoursesHomeComponent,
    data: {
        title: 'Courses'
    },
    // redirectTo: ':id',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/activeCourse'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: CoursesTableComponent,
        data: {
          model: 'Courses',
          title: 'Courses'
        },
        resolve: {
          department: ActiveDepartmentResolver,
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver
        },
        children: [
          {
            path: ':id/complete-inscription',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'Courses',
              action: 'inscription-edit'
            },
            resolve: {
              data: AdvancedFormItemResolver
            }
          }
        ]
      }
    ]
  },
  // {
  //   path: 'create',
  //   component: CoursesRootComponent,
  //   children: [
  //     {
  //       path: '',
  //       pathMatch: 'full',
  //       redirectTo: 'new'
  //     },
  //     {
  //       path: 'new',
  //       component: AdvancedFormRouterComponent,
  //       data: {
  //         maxNumberOfRemarking : 4,
  //         '$state': 1
  //       },
  //     },
  //     {
  //       path: 'newComplex',
  //       component: AdvancedFormRouterComponent,
  //       data: {
  //         maxNumberOfRemarking : 4,
  //         action: 'newComplex',
  //         '$state': 1,
  //         courseStructureType: 4
  //       },
  //     }
  //   ],
  //   resolve: {
  //     department: ActiveDepartmentIDResolver
  //   }
  // },
  {
    path: ':id',
    component: CoursesRootComponent,
    data: {
      title: 'Courses Root',
      model: 'Courses'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard/overview'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Courses.Overview'
        },
        children: [
          {
            path: '',
            redirectTo: 'overview',
            pathMatch: 'full'
          },
          {
            path: 'overview',
            component: CoursesOverviewComponent,
            data: {
              title: 'Courses.Overview'
            },
            children: [
              {
                path: 'replace',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'Courses',
                  action: 'replaceCourse',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              },
              {
                path: 'replaceCourses',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'Courses',
                  action: 'specifyReplacements',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              },
              {
                path: 'change-scale',
                pathMatch: 'full',
                component: ChangeCourseGradeScaleComponent,
                outlet: 'modal',
                data: {
                  serviceQueryParams: {
                    $select: 'id, displayCode, name, courseStructureType, gradeScale, isEnabled, parentCourse'
                  }
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'general',
            component: CoursesPreviewGeneralComponent,
            data: {
              title: 'Courses.General'
            }
          },
          {
            path: 'classes',
            component: CoursesPreviewClassesComponent,
            data: {
              title: 'Courses.Classes'
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'CourseClasses',
                  action: 'new',
                  title: null,
                  closeOnSubmit: true
                },
                resolve: {
                  course: AdvancedFormParentItemResolver,
                  title: CourseTitleResolver,
                  department: ActiveDepartmentResolver,
                  year: CurrentAcademicYearResolver,
                  period: CurrentAcademicPeriodResolver
                }
              }
            ]
          },
          {
            path: 'exams',
            component: CoursesExamsComponent,
            data: {
              title: 'Courses.Exams'
            },
            resolve: {
            },
            children: [
              {
                path: 'exam/new',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'CourseExams',
                  closeOnSubmit: true,
                  action: 'new',
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  course: AdvancedFormParentItemResolver,
                  department: ActiveDepartmentResolver,
                  year: CurrentAcademicYearResolver,
                  inscriptionPeriod: CurrentAcademicPeriodResolver
                }
              }
            ]
          },
          {
            path: 'students',
            component: CoursesStudentsComponent,
            data: {
              title: 'Courses.Students',
              model: 'StudentCourses',
              list: 'courseStudents'
            },
            resolve: {
              searchConfiguration: SearchConfigurationResolver,
              tableConfiguration: TableConfigurationResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: 'study-programs',
            component: CoursesStudyProgramsComponent,
            data: {
              title: 'Courses.StudyPrograms'
            }
          },
          {
            path: 'update-actions',
            component: CourseUpdateResultsComponent,
            data: {
              title: 'Courses.Actions',
              model: 'StudentCourseUpdateResults'
            },
            resolve: {
              tableConfiguration: TableConfigurationResolver,
              searchConfiguration: SearchConfigurationResolver,
              department: ActiveDepartmentIDResolver
            }
          },
        ]
      },
      {
        path: 'edit',
        component: AdvancedFormRouterComponent,
        data: {
          title: 'Courses.Edit',
          action: 'edit',
          serviceQueryParams: {
            $expand: 'locales,instructor,courseCategory'
          }
        },
        resolve: {
          data: AdvancedFormItemWithLocalesResolver
        }
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent,
        data: {
          title: 'Courses.Edit'
        }
      }
    ]
  }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {

}
