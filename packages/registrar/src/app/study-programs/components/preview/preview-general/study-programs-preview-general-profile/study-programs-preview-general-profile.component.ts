import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-study-programs-preview-general-profile',
  templateUrl: './study-programs-preview-general-profile.component.html'
})
export class StudyProgramsPreviewGeneralProfileComponent implements OnInit, OnDestroy {

  public model: any;
  public studyProgramID: any;
  private subscription: Subscription;

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.model = await this._context.model('StudyPrograms')
        .where('id').equal(this.studyProgramID)
        .expand('source,department,studyLevel,gradeScale,feeCurrency,discountCategories($expand=locale,label),info($expand=studyTitleType,specializationTitleType),currentPeriod($expand=locale)')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


}
