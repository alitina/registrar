import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs-compat/Subscription';

@Component({
  selector: 'app-study-programs-preview-timetable',
  templateUrl: './study-programs-preview-timetable.component.html',
  styleUrls: ['./study-programs-preview-timetable.component.scss']
})
export class StudyProgramsPreviewTimetableComponent implements OnInit {

@Input() studyProgram: number;

public timetable: any;
public studyProgramID: any;
private subscription: Subscription;

constructor( private _activatedRoute: ActivatedRoute,
             private _context: AngularDataContext) { }

async ngOnInit() {
  this.subscription = this._activatedRoute.params.subscribe(async (params) => {
    this.studyProgramID = params.id;
    this.timetable = await this._context.model('TimetableEvents')
      .where('studyPrograms/id').equal(this.studyProgramID)
      .expand('availableEventTypes')
      .getItem();
  });
}

}
