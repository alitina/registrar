import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTimetableComponent } from './preview-timetable.component';

describe('PreviewTimetableComponent', () => {
  let component: PreviewTimetableComponent;
  let fixture: ComponentFixture<PreviewTimetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewTimetableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
