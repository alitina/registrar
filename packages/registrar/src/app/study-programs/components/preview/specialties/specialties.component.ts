import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as SPECIALTIES_LIST_CONFIG from './specialties-table.config.json';
import {AppEventService, DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';


@Component({
  selector: 'app-specialties',
  templateUrl: './specialties.component.html'
})

export class SpecialtiesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> SPECIALTIES_LIST_CONFIG;
  @ViewChild('specialties') specialties: AdvancedTableComponent;
  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _appEventService: AppEventService
  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.specialties;
      this.specialties.query = this._context.model('StudyProgramSpecialties')
        .where('studyProgram').equal(params.id)
        .orderBy('specialty')
        .prepare();
      this.specialties.config = AdvancedTableConfiguration.cast(SPECIALTIES_LIST_CONFIG);
      this.specialties.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.specialties.fetch(true);
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.specialties && this.specialties.selected && this.specialties.selected.length) {
      // get items to remove
      const items = this.specialties.selected.map(item => {
        return {
          id: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('StudyPrograms.RemoveSpecialtyTitle'),
        this._translateService.instant('StudyPrograms.RemoveSpecialtyMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('studyProgramSpecialties').remove(items).then(() => {
            this._toastService.show(
              this._translateService.instant('StudyPrograms.RemoveSpecialtiesMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'StudyPrograms.RemoveSpecialtiesMessage.one' : 'StudyPrograms.RemoveSpecialtiesMessage.many')
                , { value: items.length })
            );
            this.specialties.fetch(true);
            this._appEventService.change.next({
              model: 'StudyProgramSpecialties'
            });
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }

}
