// tslint:disable: max-line-length
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyProgramsHomeComponent } from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsRoutingModule} from './study-programs.routing';
import {StudyProgramsSharedModule} from './study-programs.shared';
import { StudyProgramsTableComponent } from './components/study-programs-table/study-programs-table.component';
import {TranslateModule} from '@ngx-translate/core';
import {TablesModule} from '@universis/ngx-tables';
import { StudyProgramsRootComponent } from './components/study-programs-root/study-programs-root.component';
import { StudyProgramsPreviewComponent } from './components/preview/study-programs-preview.component';
import { StudyProgramsPreviewGeneralComponent } from './components/preview/preview-general/study-programs-preview-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { StudyProgramsPreviewCoursesComponent } from './components/preview/preview-courses/study-programs-preview-courses.component';
import { CoursesSharedModule } from '../courses/courses.shared';
import {ElementsModule} from '../elements/elements.module';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';
import { MostModule } from '@themost/angular';
import { StudyProgramsAdvancedTableSearchComponent } from './components/study-programs-table/study-programs-advanced-table-search.component';
import { StudyProgramsCoursesAdvancedTableSearchComponent } from './components/preview/preview-courses/study-programs-courses-advanced-table-search.component';
import {RouterModalModule} from '@universis/common/routing';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {AdvancedFormsModule} from '@universis/forms';
import { StudyProgramsPreviewGeneralProfileComponent } from './components/preview/preview-general/study-programs-preview-general-profile/study-programs-preview-general-profile.component';
import { StudyProgramsPreviewGeneralSpecialtiesComponent } from './components/preview/preview-general/study-programs-preview-general-specialties/study-programs-preview-general-specialties.component';
import { StudyProgramsPreviewGeneralStatisticsComponent } from './components/preview/preview-general/study-programs-preview-general-statistics/study-programs-preview-general-statistics.component';
import {ChartsModule} from 'ng2-charts';
import { StudyProgramsPreviewGeneralStatisticsStudentStatusComponent } from './components/preview/preview-general/study-programs-preview-general-statistics-student-status/study-programs-preview-general-statistics-student-status.component';
import {TooltipModule} from 'ngx-bootstrap';
import {RulesModule} from '../rules';
import { SpecialtiesComponent } from './components/preview/specialties/specialties.component';
// tslint:disable-next-line:import-spacing
import  {GroupsComponent} from './components/preview/groups/groups.component';
// tslint:disable-next-line:import-spacing
import  {GroupCoursesComponent} from './components/preview/group-courses/group-courses.component';
// tslint:disable-next-line:import-spacing
import  {GroupCoursesGeneralComponent} from './components/preview/group-courses/group-courses-general/group-courses-general.component';
import {GroupCoursesDetailsComponent} from './components/preview/group-courses/group-courses-details/group-courses-details.component';
// tslint:disable-next-line:import-spacing max-line-length
import {GroupAddCourseComponent} from './components/preview/group-courses/group-courses-details/group-add-course.component';
import {CopyProgramComponent} from './components/preview/copy-program/copy-program.component';
import {StudyProgramsPreviewActionsComponent} from './components/preview/preview-general/study-programs-preview-actions/study-programs-preview-actions.component';
import {PreviewSemesterRulesComponent} from './components/preview/preview-semester-rules/preview-semester-rules.component';
import { PreviewTimetableComponent } from './components/preview/preview-timetable/preview-timetable.component';
import { ClassesModule } from '../classes/classes.module';
import { StudyProgramsPreviewTimetableComponent } from './components/preview/preview-general/study-programs-preview-timetable/study-programs-preview-timetable.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    StudyProgramsSharedModule,
    StudyProgramsRoutingModule,
    SharedModule,
    FormsModule,
    CoursesSharedModule,
    ElementsModule,
    MostModule,
    RouterModalModule,
    RegistrarSharedModule,
    AdvancedFormsModule,
    ChartsModule,
    TooltipModule.forRoot(),
    RulesModule, 
    ClassesModule
  ],
  declarations: [StudyProgramsHomeComponent, StudyProgramsTableComponent,
    StudyProgramsRootComponent, StudyProgramsPreviewComponent, StudyProgramsPreviewGeneralComponent,
    StudyProgramsPreviewCoursesComponent, ProgramCoursePreviewComponent,
    ProgramCoursePreviewGeneralComponent, StudyProgramsAdvancedTableSearchComponent,
    StudyProgramsCoursesAdvancedTableSearchComponent,
    StudyProgramsPreviewGeneralProfileComponent,
    StudyProgramsPreviewGeneralSpecialtiesComponent,
    StudyProgramsPreviewGeneralStatisticsComponent,
    StudyProgramsPreviewGeneralStatisticsStudentStatusComponent,
    SpecialtiesComponent,
    GroupsComponent,
    GroupCoursesComponent,
    GroupAddCourseComponent,
    GroupCoursesGeneralComponent,
    GroupCoursesDetailsComponent,
    CopyProgramComponent,
    StudyProgramsPreviewActionsComponent,
    PreviewSemesterRulesComponent,
    PreviewTimetableComponent,
    StudyProgramsPreviewTimetableComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudyProgramsModule { }
